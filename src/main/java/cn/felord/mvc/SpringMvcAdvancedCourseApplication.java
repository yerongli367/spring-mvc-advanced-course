package cn.felord.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dax
 */
@SpringBootApplication
public class SpringMvcAdvancedCourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcAdvancedCourseApplication.class, args);
    }

}
