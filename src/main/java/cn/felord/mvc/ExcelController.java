package cn.felord.mvc;

import cn.felord.mvc.excel.alibaba.DefaultAliExcelView;
import cn.felord.mvc.excel.alibaba.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dax
 * @since 2020/4/5 11:11
 */
@Controller
@RequestMapping("/excel")
public class ExcelController {

    private DefaultAliExcelView defaultAliExcelView;

    public ExcelController(DefaultAliExcelView defaultAliExcelView) {
        this.defaultAliExcelView = defaultAliExcelView;
    }

    @GetMapping("/view")
    public ModelAndView excelView() {

        UserDTO userDTO = new UserDTO();
        userDTO.setId("1");
        userDTO.setName("zhangshan");
        UserDTO userDTO1 = new UserDTO();
        userDTO1.setId("2");
        userDTO1.setName("lisi");
        UserDTO userDTO2 = new UserDTO();
        userDTO2.setId("3");
        userDTO2.setName("wangwu");
        List<UserDTO> objects = new ArrayList<>();
        objects.add(userDTO);
        objects.add(userDTO1);
        objects.add(userDTO2);


        return new ModelAndView(defaultAliExcelView, "userData", objects);
    }

}
