package cn.felord.mvc.excel;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author dax
 * @since 2020/4/5 10:40
 */
@Component
public class DefaultXlsView extends AbstractXlsView {

    private BiConsumer<Map<String, Object>, Workbook> dataInjector;

    /**
     *
     * @param dataInjector the data injector
     */
    public void dataInjector(BiConsumer<Map<String, Object>, Workbook> dataInjector) {
        this.dataInjector = dataInjector;
    }


    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        dataInjector.accept(model, workbook);
    }


}
