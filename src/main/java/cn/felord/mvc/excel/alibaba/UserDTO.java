package cn.felord.mvc.excel.alibaba;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author dax
 * @since 2020/4/8 21:59
 */
@Data
public class UserDTO {
    @ExcelProperty("Id")
    private String id;
    @ExcelProperty("Name")
    private String name;
}
