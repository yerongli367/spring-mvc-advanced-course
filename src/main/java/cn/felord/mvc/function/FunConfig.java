package cn.felord.mvc.function;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.function.*;

import java.util.HashMap;

/**
 * @author dax
 * @since 2020/4/5 23:19
 */
@Configuration
public class FunConfig {



    @Bean
    public RouterFunction<ServerResponse> routes(){
        return RouterFunctions.route().GET("/hello", request -> {
            HashMap<String, String> stringStringHashMap = new HashMap<>();
             stringStringHashMap.put("hello","world");
            return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(stringStringHashMap);
        }).build();
    }



}
